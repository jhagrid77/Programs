local term = require("term")
local fs = require("filesystem")
local comp = require("component")
local computer = require("computer")
local key = require("keyboard")
local event = require("event")
local thread = require("thread")
local gpu = comp.gpu

local resx, resy = gpu.getResolution()
local halfresx = math.floor(resx/2)
local halfresy = math.floor(resy/2)
local quarterresx = math.floor(halfresx/2)
local quarterresy = math.floor(halfresy/2)

local src = 0

function printMenu()

  term.clear()
  if src == 0 then
    term.setCursor(quarterresx+2,halfresy-2)
    print("[Continue Normally]")
  else
    term.setCursor(quarterresx+3,halfresy-2)
    print("Continue Normally")
  end
  if src == 1 then
    term.setCursor(quarterresx+2,halfresy) -- Will need to change this
    print("[Display Information]")
  else
    term.setCursor(quarterresx+3,halfresy)
    print("Display Information")
  end
  if src == 2 then
    term.setCursor(quarterresx+2,halfresy+2)
    print("[Shutdown Computer]")
  else
    term.setCursor(quarterresx+3,halfresy+2)
    print("Shutdown Computer")
  end

  local _,_,_,kn = event.pull("key_down")

  if kn == 208 then
    if src == 0 then
      src = src +1
    elseif src == 1 then
      src = src +1
    end
  elseif kn == 200 then
    if src == 1 then
      src = src -1
    elseif src == 2 then
      src = src -1 
    end
  end
  if kn == 28 then
    if src == 0 then
      term.clear()
      os.execute("/etc/motd")
      os.exit()
    elseif src == 1 then
      printInfo()
    elseif src == 2 then
      computer.shutdown()
    end
  end
  printMenu()
end

function printBarGraph()
  local barMaxCharacters = 9
  local barValue = computer.freeMemory()
  local barMaxValue = computer.totalMemory()
  local barString = "["
  local barValueCharacterCount = math.floor((barValue/barMaxValue)*barMaxCharacters)
  for i=1, barMaxCharacters, 1 do
    if i <= barValueCharacterCount then
      barString = barString .. "#"
    else
      barString = barString .. " "
    end
  end
  barString = barString .. "]"
  term.setCursor(quarterresx+2, halfresy+2)
  print("Free: " .. barString)
end

function printInfo()
  id = event.timer(1,printMem,math.huge)
  term.clear()
  term.setCursor(quarterresx+2,halfresy-2)
  print("Free Memory: " .. math.floor(computer.freeMemory()/1024) .. " KB")
  term.setCursor(quarterresx+2,halfresy)
  print("Installed memory: " .. math.floor(computer.totalMemory()/1024) .. " KB")
  local bar = thread.create(function()
    while true do
      os.sleep(0.5) 
      printBarGraph() 
    end
  end)
  term.setCursor(quarterresx+5,halfresy+4)
  event.pull("key_down")
  event.cancel(id)
  bar:kill()
end

function printMem()
  term.setCursor(quarterresx+2,halfresy-2)
  term.clearLine()
  term.setCursor(quarterresx+2,halfresy-2)
  print("Free Memory: " .. math.floor(computer.freeMemory()/1024) .. " KB")
end

printMenu()