# Programs
This is my Repository for OpenComputers Programs

This repository is updated with new/updated programs as often as possible.

- LuaTutorial is not close to being done and will probably be re-written from what little there is.
- LuaDU is not finished yet either.

Interested in giving me advice, helping fix a program or simply linking me to good tutorials to learn Lua for OpenComputers? Go ahead and let me know!