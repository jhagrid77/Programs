local term = require("term")
local fs = require("filesystem")
local comp = require("component")
local computer = require("computer")
local shell = require("shell")
local serial = require("serialization")
local cfs = comp.filesystem
local gpu = comp.gpu
local args, ops = shell.parse(...)
local pwd = os.getenv("PWD")
local resx, resy = gpu.getResolution()
local usage = comp.filesystem.spaceUsed()
local usagemeasure = " Bytes"
local version = "0.01"

opCount = 0
for i, v in pairs(ops) do
  opCount = opCount + 1
end

if usage >= 1024 then
  usage = math.ceil(usage/1024)
  usagemeasure = " Kilobytes"
  if usage >= 1024 then
    usage = math.ceil(usage/1024)
    usagemeasure = " Megabytes"
    if usage >= 1024 then
      usage = math.ceil(usage/1024)
      usagemeasure = " Gigabytes"
    end
  end
end

function measurement(size)
  if size >= 1024 then
    size = math.ceil(size/1024)
    sizemeasure = " Kilobytes"
    if size >= 1024 then
      size = math.ceil(size/1024)
      sizemeasure = " Megabytes"
      if size >= 1024 then
        size = math.ceil(size/1024)
        sizemeasure = " Gigabytes"
      end
    end
  end
end
  
function printHeader()
  term.clear()
  term.setCursor(1, 1)
  print("luadu " .. version .. " - Arrow keys: navigate. ?: display help")
end

function printFooter()
  term.setCursor(1, resy-1)
  print("Total disk usage: " .. usage .. usagemeasure)
end

if #args >= 2 then
  print("Please specify ONE directory to begin scanning from.")
elseif #args == 1 then
  directory = args[1]
elseif #args == 0 then
  directory = pwd
end

function listDirectory()
  list = cfs.list(directory)
end

if opCount > 1 then
  print("Please only specify ONE option to run!")
elseif opCount == 1 and (ops.h or ops['help']) then
  print("luadu <options> <directory>")
  print(" -h    This help message")
  print(" -v    Print version")
elseif opCount == 1 and (ops.v or ops['version']) then
  print("luadu " .. version)
elseif opCount == 1 and ops[1] ~= (ops.h or ops['help'] or ops.v or ops['version']) then
  print("luadu: Unknown option '" .. ops[1] .. "'.")
  print("Avaliable options are: ")
  print(" -h    This help message")
  print(" -v    Print version")
end

printHeader()
listDirectory()  
printFooter()