local shell = require ("shell")
local fs = require("filesystem")

local arg = shell.parse(...)

if arg[1] ~= nil and fs.exists("/home/" .. arg[1]) then
  os.execute("/home/" .. arg[1] .. "/" .. arg[1] .. ".lua")
elseif arg[1] ~= nil and fs.exists("/home/" .. arg[1] .. ".lua") then
  os.execute("/home/" .. arg[1] .. ".lua")
elseif arg[1] == nil then
  print("Please specify a program to run.")
else
  print("The program specified does not exist")
end