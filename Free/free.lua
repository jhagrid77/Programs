local computer = require("computer")
local total = computer.totalMemory()
local shell = require("shell")
local max = 0
local args, op = shell.parse(...)

if (op.h or op['human-readable']) then
  for _=1,40 do
    max = math.floor(math.max(max, computer.freeMemory())/1024)
    os.sleep(0) -- invokes gc
  end
  total = math.floor(computer.totalMemory()/1024)
  io.write(string.format("Total%12d KB\nUsed%13d KB\nFree%13d KB\n", total, total - max, max))
elseif (op['help']) then
  io.write("-h or --human-readable will show how much RAM in total, and free there is in Kilobytes (KB)")
else
  for _ =1,40 do
     max = math.max(max, computer.freeMemory())
     os.sleep(0) -- invokes gc
  end
  io.write(string.format("Total%12d\nUsed%13d\nFree%13d\n", total, total - max, max))
end